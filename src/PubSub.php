<?php

declare(strict_types=1);

namespace AutoAction\PubSub;

use Google\Cloud\PubSub\PubSubClient;
use Google\Cloud\PubSub\Topic;

/**
 * PubSub
 *
 * @see https://cloud.google.com/pubsub/docs
 *      Documentação completa do PubSub no google
 *
 * @package AutoAction\PubSub
 * @date    06/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class PubSub
{
    /**
     * @var PubSubClient
     */
    private $client;

    /**
     * @var Topic
     */
    private $topic;

    /**
     * ID do Projeto
     *
     * @var string $projectId
     */
    private $projectId;

    /**
     * Campo data do PubSub
     *
     * @var
     */
    private static $data;

    /**
     * Campo attributes do PubSub
     *
     * @var
     */
    private static $attributes;

    /**
     * @var ID da mensagem do PubSub
     */
    private static $messageId;

    /**
     * PubSub constructor.
     *
     * @param string      $projectId
     * @param string|null $certificationPath
     */
    public function __construct(string $projectId, string $certificationPath = null)
    {
        $this->projectId = $projectId;

        if ($certificationPath == null || empty($certificationPath)) {
            $config = [
                "projectId" => $projectId
            ];
        } else {
            $config = [
                "projectId" => $projectId,
                "keyFilePath" => $certificationPath
            ];
        }

        $this->client = new PubSubClient($config);
    }

    /**
     * Tópico sendo trabalhado
     *
     * @param string $topicName
     * @return $this
     */
    public function topic(string $topicName)
    {
        $prefix      = "projects/{$this->projectId}/topics";
        $topic       = "{$prefix}/{$topicName}";
        $this->topic = $this->client->topic($topic);

        return $this;
    }

    /**
     * Publica em um determinado tópico
     *
     * @param array  $attributes
     * @param string $data
     * @return array
     */
    public function publish(array $attributes, $data = 'default')
    {
        return $this->topic->publish(['data' => $data, 'attributes' => $attributes]);
    }

    /**
     * Dados recebidos no getPush
     * @return mixed
     */
    public static function getData()
    {
        return self::$data;
    }

    /**
     * Dados recebidos no getPush
     * @return array
     */
    public static function getAttributes()
    {
        return self::$attributes;
    }

    /**
     * Recupera os dados enviados pelo pubSub
     *
     * @return array|bool
     */
    public static function getPush()
    {
        $request     = file_get_contents('php://input');
        $requestData = json_decode($request, true);
        $message     = $requestData['message'];
        $data        = isset($message['data']) ? base64_decode($message['data']) : false;
        $attributes  = isset($message['attributes']) ? $message['attributes'] : false;
        $messageId   = isset($message['messageId']) ? $message['messageId'] : false;

        if (!$data && $attributes) {
            return false;
        }

        self::$data       = $data;
        self::$attributes = $attributes;
        self::$messageId  = $messageId;

        return [
            "data"       => $data,
            "attributes" => $attributes,
            "messageId"  => $messageId,
        ];
    }

}